import java.util.ArrayList;

class Book{
    private String title;
    private String author;
    private int year;
    private String isbn;
    public Book(String title,String author,int year,String isbn){
        this.title = title;
        this.author = author;
        this.year = year;
        this.isbn = isbn;

    }


    public String getBookInfo() {
        return "Title: " + title + ", Author: " + author + ", Year: " + year + ", ISBN: " + isbn;
    }

    public String getISBN(){
        return this.isbn;
    }

    public String getAuthor(){
        return this.author;
    }

    public String getTitle(){
        return this.title;
    }
    
    public int getPushlishyear(){
        return this.year;
    }
}



class Library{
    ArrayList<Book> books; 

    public Library(){
        books = new ArrayList<>();
    }
    public void addBook(Book book){
        books.add(book);
    }

    public void removeBook(String isbn){
        books.removeIf(book -> book.getISBN().equals(isbn));
    }
    
    public ArrayList<Book> getBooksPublishedAfterYear(int year){
        ArrayList<Book> result = new ArrayList<>();
        for(Book book:books){
            if(book.getPushlishyear() > year){
                result.add(book);
            }
        }

        return result;
    }

    public ArrayList<String> getAuthorsOfBooksPublishedBeforeYear(int year){
        ArrayList<String> result1 = new ArrayList<>();
        for(Book book: books){
            if(book.getPushlishyear() < year){
                result1.add(book.getAuthor());
            }

        }
        return result1; 
    }
}





